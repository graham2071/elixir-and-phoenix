defmodule Cards do
  @moduledoc """
  Provides methods for creating and handling deck of cards.

  ## Examples

      iex> deck = Cards.create_deck
      iex> deck = Cards.shuffle(deck)
      iex> {hand, _rest_of_deck} = Cards.deal(deck, 5)
      iex> Cards.save(hand, "data/hand.bin")
      iex> {status, hand} = Cards.load("data/hand.bin")
      iex> {status, hand}
  """

  @typedoc """
  A card is represented with a single string.
  """
  @type card() :: String.t()

  @typedoc """
  A `deck` is a list of `card`s.
  """
  @type deck() :: list(card())

  @doc """
  Creates a deck of cards
  """
  @spec create_deck() :: deck()
  def create_deck() do
    values = ["Ace", "Two", "Three", "Four", "Five"]
    suits = ["Spades", "Clubs", "Hearts", "DiamondS"]

    for value <- values, suit <- suits do
      "#{value} of #{suit}"
    end
  end

  @spec shuffle(deck()) :: deck()
  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  @doc """
  Returns true if a `card` is contained inside a `deck`.

  ## Examples

      iex> deck = Cards.create_deck()
      iex> Cards.contains?(deck, "Five of Hearts")
      true
      iex> Cards.contains?(deck, "Six of Hearts")
      false
  """
  @spec contains?(deck(), card()) :: boolean
  def contains?(deck, card) do
    Enum.member?(deck, card)
  end

  @doc """
  Divides a deck into a hand and the remainder of the deckj. The `hand_size` argument indicates how many cards should be in the hand.

  ## Examples

      iex> deck = Cards.create_deck()
      iex> {hand, _deck} = Cards.deal(deck, 1)
      iex> hand
      ["Ace of Spades"]
  """
  @spec deal(deck(), integer) :: {deck(), deck()}
  def deal(deck, size) do
    Enum.split(deck, size)
  end

  @spec save(deck(), String.t()) :: :ok | {:error, File.posix()}
  def save(deck, filename) do
    binary = :erlang.term_to_binary(deck)
    File.write(filename, binary)
  end

  @spec load(String.t()) :: {:ok, deck()} | {:error, String.t()}
  def load(filename) do
    case File.read(filename) do
      {:ok, binary} -> {:ok, :erlang.binary_to_term(binary)}
      {:error, error} -> {:error, "Invalid file #{filename}: #{error}"}
    end
  end

  @doc """
  create_hand creates a new deck, shuffle, and deals cards.
  """
  @spec create_hand(integer) :: {deck(), deck()}
  def create_hand(hand_size) do
    create_deck()
    |> shuffle
    |> deal(hand_size)
  end
end
