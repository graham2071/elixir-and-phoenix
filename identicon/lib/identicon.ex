defmodule Identicon do
  @moduledoc """
  `Identicon` generates unique avatar icons.

  An identicon is a square 5x5 pixels. This is symetric.
  An identicon is generated based on an input string. The generation process is deterministic.
  """

  # Number of pixels per square of the image. The final image is 5x5 squares.
  @square_length 50

  @type image :: %Identicon.Image{
          hex: nil | list(any()),
          color: nil | {byte, byte, byte},
          grid: nil | list(list(byte)),
          pixel_map: nil | list({})
        }

  @doc """
  Creates an avatar icon from an image and saves it to `img/input.png`.

  ## Examples

      iex> Identicon.main("James")
      :ok
  """

  @spec main(String.t()) :: :ok | {:error, File.posix()}
  def main(input) do
    input
    |> hash
    |> pick_color
    |> build_grid
    |> filter_odd_squares
    |> build_pixel_map
    |> draw_image
    |> save_image(input)
  end

  @doc """
  Hashess an input string.

  ## Examples

      iex> Identicon.hash("James")
      %Identicon.Image{hex: [213, 46, 50, 243, 169, 106, 100, 120, 104, 20, 174, 155, 82, 121, 251, 229]}

  """
  @spec hash(String.t()) :: image()
  def hash(input) do
    hex =
      :crypto.hash(:md5, input)
      |> :binary.bin_to_list()

    %Identicon.Image{hex: hex}
  end

  @doc """
  Fills up the rgb colors with the 3 first values of hex list.

  ## Examples

      iex> Identicon.pick_color(%Identicon.Image{hex: [213, 46, 50, 243, 169, 106, 100, 120, 104, 20, 174, 155, 82, 121, 251, 229]})
      %Identicon.Image{hex: [213, 46, 50, 243, 169, 106, 100, 120, 104, 20, 174, 155, 82, 121, 251, 229], color: {213, 46, 50}}

  """
  @spec pick_color(image()) :: image()
  def pick_color(%Identicon.Image{hex: hex_list} = image) do
    [r, g, b | _tail] = hex_list
    %Identicon.Image{image | color: {r, g, b}}
  end

  @doc """
  Creates the grid of bytes. Each item is indexed.

  ## Examples

      iex> Identicon.build_grid(%Identicon.Image{hex: [213, 46, 50, 243, 169, 106, 100, 120, 104, 20, 174, 155, 82, 121, 251, 229], color: [213, 46, 50]})
      %Identicon.Image{
        hex: [213, 46, 50, 243, 169, 106, 100, 120, 104, 20, 174, 155, 82, 121, 251, 229],
        color: [213, 46, 50],
        grid: [{213, 0},
                {46, 1},
                {50, 2},
                {46, 3},
                {213, 4},
                {243, 5},
                {169, 6},
                {106, 7},
                {169, 8},
                {243, 9},
                {100, 10},
                {120, 11},
                {104, 12},
                {120, 13},
                {100, 14},
                {20, 15},
                {174, 16},
                {155, 17},
                {174, 18},
                {20, 19},
                {82, 20},
                {121, 21},
                {251, 22},
                {121, 23},
                {82, 24}]
      }
  """
  @spec build_grid(image()) :: image()
  def build_grid(%Identicon.Image{hex: hex_list} = image) do
    grid =
      hex_list
      |> Enum.chunk_every(3, 3, :discard)
      |> Enum.map(&mirror_row/1)
      |> List.flatten()
      |> Enum.with_index()

    %Identicon.Image{image | grid: grid}
  end

  @doc """
  Returns a row mirrored with the latest element.

  ## Examples

      iex> Identicon.mirror_row([213, 46, 50])
      [213, 46, 50, 46, 213]

  """
  @spec mirror_row(list(byte)) :: list(byte)
  def mirror_row(row) do
    [first, second | _tail] = row
    row ++ [second, first]
  end

  @doc """
  Removed from grid all odd grid square, and keep even ones.

  ## Examples

      iex> Identicon.filter_odd_squares(%Identicon.Image{grid: [{213, 0}, {46, 1}, {50, 2}, {46, 3}, {213, 4}, {243, 5}, {169, 6}]})
      %Identicon.Image{hex: nil, color: nil, grid: [{46, 1}, {50, 2}, {46, 3}]}

  """
  @spec filter_odd_squares(image()) :: image()
  def filter_odd_squares(%Identicon.Image{grid: grid} = image) do
    grid =
      Enum.filter(
        grid,
        fn {b, _index} ->
          rem(b, 2) == 0
        end
      )

    %Identicon.Image{image | grid: grid}
  end

  @doc """
  Creates the pixel map (colored squares) from grid.

  ## Examples

      iex> Identicon.build_pixel_map(%Identicon.Image{grid: [{213, 0}, {46, 1}, {50, 2}, {46, 3}, {213, 4}, {243, 5}, {169, 6}]})
      %Identicon.Image{
        grid: [{213, 0}, {46, 1}, {50, 2}, {46, 3}, {213, 4}, {243, 5}, {169, 6}],
        pixel_map: [{{0, 0}, {50, 50}}, {{50, 0}, {100, 50}}, {{100, 0}, {150, 50}}, {{150, 0}, {200, 50}}, {{200, 0}, {250, 50}}, {{0, 50}, {50, 100}}, {{50, 50}, {100, 100}}]
      }

  """
  @spec build_pixel_map(image()) :: image()
  def build_pixel_map(%Identicon.Image{grid: grid} = image) do
    pixel_map =
      Enum.map(grid, fn {_b, index} ->
        row_index = rem(index, 5)
        col_index = div(index, 5)

        top_left = {row_index * @square_length, col_index * @square_length}

        bottom_right = {(row_index + 1) * @square_length, (col_index + 1) * @square_length}

        {top_left, bottom_right}
      end)

    %Identicon.Image{image | pixel_map: pixel_map}
  end

  @spec draw_image(image()) :: binary
  def draw_image(%Identicon.Image{color: color, pixel_map: pixel_map}) do
    image = :egd.create(5 * @square_length, 5 * @square_length)
    fill = :egd.color(color)

    Enum.each(pixel_map, fn {top_left, bottom_right} ->
      :egd.filledRectangle(image, top_left, bottom_right, fill)
    end)

    :egd.render(image)
  end

  @spec save_image(binary, String.t()) :: :ok | {:error, File.posix()}
  def save_image(binary, filename) do
    File.write("img/#{filename}.png", binary)
  end
end
